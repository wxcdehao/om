package com.zero2oneit.mall.goods.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zero2oneit.mall.goods.service.GroupRuleService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
@RestController
@RequestMapping("/admin/group")
public class GroupRuleController {

    @Autowired
    private GroupRuleService groupRuleService;

}
