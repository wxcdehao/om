package com.zero2oneit.mall.goods.service.impl;

import com.zero2oneit.mall.common.bean.goods.GroupRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zero2oneit.mall.goods.mapper.GroupRuleMapper;
import com.zero2oneit.mall.goods.service.GroupRuleService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
@Service
public class GroupRuleServiceImpl extends ServiceImpl<GroupRuleMapper, GroupRule> implements GroupRuleService {

    @Autowired
    private GroupRuleMapper groupRuleMapper;

}