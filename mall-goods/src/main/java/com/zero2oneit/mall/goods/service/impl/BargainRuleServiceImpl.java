package com.zero2oneit.mall.goods.service.impl;

import com.zero2oneit.mall.common.bean.goods.BargainRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zero2oneit.mall.goods.mapper.BargainRuleMapper;
import com.zero2oneit.mall.goods.service.BargainRuleService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
@Service
public class BargainRuleServiceImpl extends ServiceImpl<BargainRuleMapper, BargainRule> implements BargainRuleService {

    @Autowired
    private BargainRuleMapper bargainRuleMapper;

}