package com.zero2oneit.mall.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.goods.BargainRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Tg
 * @create 2021-03-19
 * @description
 */
@Mapper
public interface BargainRuleMapper extends BaseMapper<BargainRule> {
	
}
