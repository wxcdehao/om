package com.zero2oneit.mall.common.exception;

import com.zero2oneit.mall.common.enums.LogSucceed;
import com.zero2oneit.mall.common.enums.LogType;
import com.zero2oneit.mall.common.exception.po.LogPO;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Description:全局异常处理器
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/1/11
 */
@ControllerAdvice
@Component
public class GlobalException {

   /* @Autowired
    private AmqpTemplate amqpTemplate;*/

    /**
     * 异常统一处理
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R exceptionHandler(HttpServletRequest request, Exception e) {
        //amqpTemplate.convertAndSend("mall.exception.exchange", "exception.insert", new LogPO(LogType.EXCEPTION.getMessage(), null, null, request.getRequestURI(), LogSucceed.FAIL.getMessage(), StringUtils.getExceptionMsg(e)));
        e.printStackTrace();
        return R.fail("服务器繁忙，稍后再请求...");
    }

}
