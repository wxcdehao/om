package com.zero2oneit.mall.search.mapper;


import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.index.query.QueryBuilder;

import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2021/3/17 -15:29
 */
public interface EsGoodProMapper {

    List esSelectList(String index, QueryBuilder query, Class c,String[] arr);

    boolean exists(String index,String id);

    GetResponse getById(String index, String id);

    boolean updateById(String index, String id, long seqNo,long term, String jsonObject);

    Map skuListStock(List<String> list,String index, String[] includes);

    Map proListShop(List<Map<String,String>> list,String index);

    BoostrapDataGrid esSelectCommon(String index, QueryBuilder query, Class c, Integer from, Integer size, String sort, Integer Type);

    Map yzSkuList(List<Map> list, String index);

    Map hotGood();

    Map yzProductSku(String skuIndex,String proIndex,String id, String productId);
}
