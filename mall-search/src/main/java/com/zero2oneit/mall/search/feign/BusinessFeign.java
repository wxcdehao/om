package com.zero2oneit.mall.search.feign;

import com.zero2oneit.mall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
/**
 * Description:
 *
 * @author Lee
 * @date 2021/3/17 21:32
 */
@FeignClient("business-service")
public interface BusinessFeign {

    @PostMapping("/admin/business/getByAll")
    R getByAll();

}
