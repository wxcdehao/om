package com.zero2oneit.mall.service.goods.remote;

import com.zero2oneit.mall.feign.goods.BargainRuleFeign;
import com.zero2oneit.mall.feign.goods.GroupRuleFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 远程调用商品分类（社区团购）服务
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/2/5
 */
@RestController
@RequestMapping("/remote/group")
@CrossOrigin
public class GroupRuleRemote {

    @Autowired
    private GroupRuleFeign ruleFeign;




}
