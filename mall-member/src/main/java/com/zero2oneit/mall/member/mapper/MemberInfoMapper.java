package com.zero2oneit.mall.member.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zero2oneit.mall.common.query.member.InfoQueryObject;
import com.zero2oneit.mall.common.bean.member.MemberInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * @author Sinper
 * @create 2020-07-15
 * @description
 */
@Mapper
public interface MemberInfoMapper extends BaseMapper<MemberInfo> {

    int Total(InfoQueryObject qo);

    List<HashMap<String,Object>> infoRows(InfoQueryObject qo);

    Integer queryLevel(String memberId);

    void ToVip(String memberId);

    void becomeAFan(String memberId);

    void addConsume(@Param("userId") String userId, @Param("amount") BigDecimal amount);
}
